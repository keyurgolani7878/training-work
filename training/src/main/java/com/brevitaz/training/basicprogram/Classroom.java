package com.brevitaz.training.basicprogram;

import java.util.Scanner;

/*
The total number of students in a class are 90 out of which 45 are boys.
If 50% of the total students secured grade 'A' out of which 20 are boys,
then write a program to calculate the total number of girls getting grade 'A'.
 */
public class Classroom {
    public static void main(String[] args) {
       /* Scanner scanner=new Scanner(System.in); //Value taken from user
        System.out.println("Enter the total number of students in class");
        int student=scanner.nextInt();
        System.out.println("Enter the number of boys in class");
        int boys=scanner.nextInt();
        System.out.println("Enter how many percentage of students have scored A Grade");
        int agradepercentage=scanner.nextInt();
        System.out.println("Enter the number of boys who have scored A Grade");
        int boyswithagrade=scanner.nextInt();
        int girls=student-boys;
        int girlswithagrade=  (student/(100/agradepercentage));*/
        int totalstudent = 90;
        int boys = 45;
        int girls = totalstudent - boys;
        int agradepercentage = 50;
        int boyswithagrade = 20;
        int girlswithagrade = (totalstudent * agradepercentage) / 100;
        System.out.println(girlswithagrade);
    }
}
