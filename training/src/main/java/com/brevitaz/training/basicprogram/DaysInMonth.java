package com.brevitaz.training.basicprogram;

import java.util.Scanner;
/*
Write a Java program to find the number of days in a month

    Test Data
    Input a month number: 2
    Input a year: 2016

    Expected Output :
    February 2016 has 29 days

 */

public class DaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a month number: ");
        int monthnumber = scanner.nextInt();
        System.out.print("Enter a year: ");
        int year = scanner.nextInt();
        int numberofdays = 31;

        if (monthnumber > 0 && monthnumber < 12) {
            String monthname = "Keyur";
            switch (monthnumber) {
                case 1:
                    numberofdays = 31;
                    monthname = "January";
                    break;
                case 2:
                    if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
                        numberofdays = 29;
                    } else {
                        numberofdays = 28;
                    }
                    monthname = "February";
                    break;
                case 3:
                    numberofdays = 31;
                    monthname = "March";
                    break;
                case 4:
                    numberofdays = 30;
                    monthname = "April";
                    break;
                case 5:
                    numberofdays = 31;
                    monthname = "May";
                    break;
                case 6:
                    numberofdays = 30;
                    monthname = "June";
                    break;
                case 7:
                    numberofdays = 31;
                    monthname = "July";
                    break;
                case 8:
                    numberofdays = 31;
                    monthname = "August";
                    break;
                case 9:
                    numberofdays = 30;
                    monthname = "September";
                    break;
                case 10:
                    numberofdays = 31;
                    monthname = "October";
                    break;
                case 11:
                    numberofdays = 30;
                    monthname = "November";
                    break;
                case 12:
                    numberofdays = 31;
                    monthname = "December";
                    break;

            }
            System.out.println(monthname + " " + year + " has " + numberofdays + " days");
        } else {
            System.out.println("Entered wrong information");
        }
    }
}