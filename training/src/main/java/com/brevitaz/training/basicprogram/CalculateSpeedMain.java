package com.brevitaz.training.basicprogram;

import java.util.Scanner;

/*
 Write a Java program to takes the user for a distance (in meters) and the time was taken (as three numbers: hours, minutes, seconds), and display the speed, in meters per second, kilometers per hour and miles per hour (hint: 1 mile = 1609 meters)

    Test Data
    Input distance in meters: 2500
    Input hour: 5
    Input minutes: 56
    Input seconds: 23

    Expected Output :
    Your speed in meters/second is 0.11691531
    Your speed in km/h is 0.42089513
    Your speed in miles/h is 0.26158804

 */
public class CalculateSpeedMain {
    public static void main(String[] args) {
        Float meterdistance, hour, minute, second;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input distance in meters:");
        meterdistance = scanner.nextFloat();
        System.out.print("Input hour:");
        hour = scanner.nextFloat();
        System.out.print("Input minutes:");
        minute = scanner.nextFloat();
        System.out.print("Input seconds:");
        second = scanner.nextFloat();
        CalculateSpeed(meterdistance, hour, minute, second);

    }

    public static void CalculateSpeed(float meterdistance, float hour, float minute, float second) {
        float totalhour = hour + (minute / 60) + (second / 3600);
        float totalseconds = (hour * 3600) + (minute * 60) + second;
        float metertomiles = (float) (meterdistance * 0.00062137);
        System.out.println("Your speed in meters/second is " + (meterdistance / totalseconds));
        System.out.println("Your speed in km/h is " + (meterdistance / (totalhour * 1000)));
        System.out.println("Your speed in miles/h " + (metertomiles / totalhour));
        System.out.println(hour + (minute / 60) + (second / 3600));
    }
}
