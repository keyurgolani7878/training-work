package com.brevitaz.training.basicprogram;

import java.util.Scanner;

/*
Write a Java program to solve quadratic equations (use if, else if and else)

    Test Data
    Input a: 1
    Input b: 5
    Input c: 1

    Expected Output :
    The roots are -0.20871215252208009 and -4.7912878474779195

 */
public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        double a = scanner.nextDouble();
        System.out.print("Enter b: ");
        double b = scanner.nextDouble();
        System.out.print("Enter c: ");
        double c = scanner.nextDouble();

        double discriminant = Math.pow(b, 2) - 4 * a * c;
        if (discriminant > 0) {
            double root1 = (-b + Math.pow(discriminant, 0.5)) / (2.0 * a);
            double root2 = (-b - Math.pow(discriminant, 0.5)) / (2.0 * a);
            System.out.println("The roots are " + root1 + "and " + root2);
        } else if (discriminant == 0) {
            double root1 = -b / (2.0 * a);
            System.out.println("The root is " + root1);
        } else {
            System.out.println("No root");
        }

    }

}
