package com.brevitaz.training.basicprogram;

/*
Write a program to assign a value of 100.235 to a double variable and then convert it to int.
 */
public class DoubleToInt {
    public static void main(String[] args) {
        double d = 100.235;
        int a = (int) d;//It gives a error if we don't cast it to int
        System.out.println(a);
    }
}
