package com.brevitaz.training.anotherpackage;
import com.brevitaz.training.accessspecifier.*;
public class SubclassOfProtectedModifier extends ProtectedModifier {
    public static void main(String[] args) {
        SubclassOfProtectedModifier subclassOfProtectedModifier=new SubclassOfProtectedModifier();
        subclassOfProtectedModifier.show();
    }
}
