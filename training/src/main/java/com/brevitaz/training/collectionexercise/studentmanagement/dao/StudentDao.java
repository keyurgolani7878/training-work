package com.brevitaz.training.collectionexercise.studentmanagement.dao;

import com.brevitaz.training.collectionexercise.studentmanagement.model.Student;

import java.util.HashMap;
import java.util.List;

public interface StudentDao {
    public boolean add(Student student);
    public void update(Student student);
    public boolean partialUpdate(int i,String fieldName,String fieldValue);
    public boolean delete(int rollNO);
    public HashMap<Integer,Student> getAllStudent();
    /*public void searchStudentUsingFirstName(String firstName);
    public void searchStudentUsingLastName(String lastName);
    public void searchStudentUsingContactNo(String contactNo);*/
}
