package com.brevitaz.training.collectionexercise;

import com.brevitaz.training.collectionexercise.studentmanagement.model.Student;
import com.brevitaz.training.collectionexercise.studentmanagement.service.StudentService;
import com.brevitaz.training.collectionexercise.studentmanagement.service.impl.StudentServiceImpl;

import java.util.Scanner;

public class StudentMain {
    public static void main(String[] args) {
        StudentService studentService = new StudentServiceImpl();

        while (true) {
            System.out.println("1. Add Student");
            System.out.println("2. Delete Student");
            System.out.println("3. Update Student");
            System.out.println("4. Get All Students details");
            System.out.println("5. Search Student");
            System.out.println("6. Exit");
            System.out.print("Enter your choice: ");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            int rollNO;
            String firstName = "";
            String lastName = "";
            String contactNo = "";
            Student student;

            switch (choice) {
                case 1:
                    System.out.print("Enter roll no: ");
                    rollNO = scanner.nextInt();
                    System.out.print("Enter first name : ");
                    firstName = scanner.next();
                    System.out.print("Enter last name : ");
                    lastName = scanner.next();
                    System.out.print("Enter contact number : ");
                    contactNo = scanner.next();
                    student = new Student(rollNO, firstName, lastName, contactNo);
                    studentService.addStudent(student);
                    break;
                case 2:
                    System.out.print("Enter the roll no of the student you want to delete ");
                    rollNO = scanner.nextInt();
                    student = new Student(rollNO, firstName, lastName, contactNo);
                    studentService.deleteStudent(student);
                    break;
                case 3:

                    System.out.println("1. First name");
                    System.out.println("2. Last name");
                    System.out.println("3. contact no");
                    System.out.println("4. whole student");
                    System.out.print("Enter what you want to update: ");
                    int updateType = scanner.nextInt();
                    switch (updateType) {
                        case 1:
                            System.out.print("Enter the roll no of student of which you want to update details: ");
                            rollNO = scanner.nextInt();
                            System.out.print("Enter the new value for firstname: ");
                            firstName = scanner.next();
                            studentService.partialUpdate(rollNO, "firstname", firstName);
                            break;
                        case 2:
                            System.out.print("Enter the roll no of student of which you want to update details: ");
                            rollNO = scanner.nextInt();
                            System.out.println("ENter the new value for lastname: ");
                            lastName = scanner.next();
                            studentService.partialUpdate(rollNO, "lastname", lastName);
                            break;
                        case 3:
                            System.out.print("Enter the roll no of student of which you want to update details: ");
                            rollNO = scanner.nextInt();
                            System.out.print("ENter the new value for contactno: ");
                            contactNo = scanner.next();
                            studentService.partialUpdate(rollNO, "contactno", contactNo);
                            break;
                        case 4:
                            System.out.print("Enter roll no: ");
                            rollNO = scanner.nextInt();
                            System.out.print("Enter first name : ");
                            firstName = scanner.next();
                            System.out.print("Enter last name : ");
                            lastName = scanner.next();
                            System.out.print("Enter contact number : ");
                            contactNo = scanner.next();
                            student = new Student(rollNO, firstName, lastName, contactNo);
                            studentService.updateStudent(student);
                            break;
                        default:
                            System.out.println("Enter right value");
                    }

                case 4:
                    studentService.getAllStudent();
                    break;
                case 5:
                    System.out.println("Enter the value using which you want to search");
                    System.out.println("1. Using Roll no");
                    System.out.println("2. Using Firstname");
                    System.out.println("3. Using Lastname");
                    System.out.println("4. Contact no");
                    int searchChoice = scanner.nextInt();
                    switch (searchChoice) {
                        case 1:
                            System.out.print("Enter roll no: ");
                            int searchedRollNO = scanner.nextInt();
                            studentService.searchStudentUsingRollNo(searchedRollNO);
                            break;
                        case 2:
                            System.out.print("Enter firstname: ");
                            String searchedFirstName = scanner.next();
                            studentService.searchStudentUsingFirstName(searchedFirstName);
                            break;
                        case 3:
                            System.out.print("Enter lastname: ");
                            String searchedLastName = scanner.next();
                            studentService.searchStudentUsingLastName(searchedLastName);
                            break;
                        case 4:
                            System.out.print("Enter contact no: ");
                            String searchedContactNO = scanner.next();
                            studentService.searchStudentUsingContactNo(searchedContactNO);
                            break;
                        default:
                            System.out.println("you have entered a wrong choice start again");
                            break;
                    }
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Enter correct choice");
                    break;
            }

        }

    }
}
