package com.brevitaz.training.collectionexercise.studentmanagement.service;

import com.brevitaz.training.collectionexercise.studentmanagement.model.Student;

public interface StudentService {
    public void addStudent(Student student);

    public void updateStudent(Student student);

    public void partialUpdate(int i, String fieldName, String fieldValue);

    public void deleteStudent(int rollNo);

    public void getAllStudent();

    public void searchStudent(String filedName,String fielValue);

    /*public void searchStudentUsingFirstName(String firstName);

    public void searchStudentUsingLastName(String lastName);

    public void searchStudentUsingContactNo(String contactNo);

    public void searchStudentUsingRollNo(int rollNo);*/
}

