package com.brevitaz.training.collectionexercise.studentmanagement.service.impl;

import com.brevitaz.training.collectionexercise.studentmanagement.dao.StudentDao;
import com.brevitaz.training.collectionexercise.studentmanagement.dao.impl.StudentDaoImpl;
import com.brevitaz.training.collectionexercise.studentmanagement.service.StudentService;
import com.brevitaz.training.collectionexercise.studentmanagement.model.Student;

import java.util.Map;

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao = new StudentDaoImpl();
   // List<Student> allStudents = studentDao.getAllStudent();
    Map<Integer,Student> studentMap=studentDao.getAllStudent();
    @Override
    public void addStudent(Student student) {
        if (!studentMap.containsKey(student.getRollNo()))
        {
            studentDao.add(student);
        }
        else
        {
            System.out.println("Student already exist");
        }
    }

    @Override
    public void deleteStudent(int rollNo) {
        if (studentMap.containsKey(rollNo))
        {
          studentDao.delete(rollNo);
        }
        else
        {
            System.out.println("No student Exist with a given roll No");
        }
    }

    @Override
    public void updateStudent(Student student) {
        if (studentMap.containsKey(student.getRollNo()))
        {
            studentDao.update(student);
        }
        else
        {
            System.out.println("Infromation is not updated because no student exist with given roll no ");
        }
    }

    @Override
    public void partialUpdate(int rollNo, String fieldName, String fieldValue) {
        if (studentMap.containsKey(rollNo))
        {
            if (fieldName.toLowerCase().replaceAll(" ", "").equals("firstname")) {
                studentDao.partialUpdate(rollNo, fieldName, fieldValue);
            } else if (fieldName.toLowerCase().replaceAll(" ", "").equals("lastname")) {
                studentDao.partialUpdate(rollNo, fieldName, fieldValue);
            } else if (fieldName.toLowerCase().replaceAll(" ", "").equals("contactno")) {
                studentDao.partialUpdate(rollNo, fieldName, fieldValue);
            } else {
                System.out.println("Enter right field name");
            }
        }
    }

    @Override
    public void getAllStudent() {
        if (studentMap.isEmpty()) {
            System.out.println("There is no student record");
        } else {
            for (Student s : studentDao.getAllStudent().values()) {
                System.out.println("Key= "+s.getRollNo()+" Values= "+s.getRollNo() + " " + s.getFirstName() + " " + s.getLastName() + " " + s.getContactNo());
            }
        }
    }

    public void searchStudent(String searchedFieldName, String searchedfieldValue) {
        if (searchedFieldName.toLowerCase().replaceAll("\\s", "").equals("rollno")) {
            int rollNoInInt = Integer.parseInt(searchedfieldValue);
            boolean isRollNoExist = false;
            for (Student s : studentMap.values()) {
                if (rollNoInInt == s.getRollNo()) {
                    System.out.println(s.getRollNo() + " " + s.getFirstName() + " " + s.getLastName() + " " + s.getContactNo());
                    isRollNoExist = true;
                }
            }
            if (!isRollNoExist) {
                System.out.println("No student exist with a given roll no");
            }
        } else if (searchedFieldName.toLowerCase().replaceAll("\\s", "").equals("firstname")) {
            boolean isFirstNameExist = false;
            for (Student s : studentMap.values()) {
                if (searchedfieldValue.equals(s.getFirstName())) {
                    System.out.println(s.getRollNo() + " " + s.getFirstName() + " " + s.getLastName() + " " + s.getContactNo());
                    isFirstNameExist = true;
                }
            }
            if (!isFirstNameExist) {
                System.out.println("No student exist with a given first name");
            }
        } else if (searchedFieldName.toLowerCase().replaceAll("\\s", "").equals("lastname")) {
            boolean isLastNameExist = false;
            for (Student s : studentMap.values()) {
                if (searchedfieldValue.equals(s.getLastName())) {
                    System.out.println(s.getRollNo() + " " + s.getFirstName() + " " + s.getLastName() + " " + s.getContactNo());
                    isLastNameExist = true;
                }
            }
            if (!isLastNameExist) {
                System.out.println("No student exist with a given last name");
            }
        } else if (searchedFieldName.toLowerCase().replaceAll("\\s", "").equals("contactno")) {
            boolean isContactNoExist = false;
            for (Student s : studentMap.values()) {
                if (searchedfieldValue.equals(s.getContactNo())) {
                    System.out.println(s.getRollNo() + " " + s.getFirstName() + " " + s.getLastName() + " " + s.getContactNo());
                    isContactNoExist = true;
                }
            }
            if (!isContactNoExist) {
                System.out.println("No student exist with a given contact no name");
            }
        } else {
            System.out.println("Enter correct value for fieldname");
        }

    }
}
