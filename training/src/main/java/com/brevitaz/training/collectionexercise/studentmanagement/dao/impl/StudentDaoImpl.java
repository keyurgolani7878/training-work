package com.brevitaz.training.collectionexercise.studentmanagement.dao.impl;

import com.brevitaz.training.collectionexercise.studentmanagement.dao.StudentDao;
import com.brevitaz.training.collectionexercise.studentmanagement.data.StudentData;
import com.brevitaz.training.collectionexercise.studentmanagement.model.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StudentDaoImpl implements StudentDao {
    //List<Student> studentDatabase = new StudentData().getStudents();
    HashMap<Integer,Student> studentDatabaseMap=new StudentData().getStudentMap();
    Set<Integer> rollNoSet=studentDatabaseMap.keySet();

    @Override
    public boolean add(Student student) {
        studentDatabaseMap.put(student.getRollNo(),student);
        System.out.println("Student added");
        return true;
    }

    @Override
    public void update(Student student) {
        studentDatabaseMap.replace(student.getRollNo(),student);
        System.out.println("student information Updated");
        /*for (Student s : studentDatabase) {
            if (s.getRollNo() == student.getRollNo()) {
                int i = studentDatabase.indexOf(s);
                studentDatabase.set(i, student);
            }
        }*/
    }

    @Override
    public boolean partialUpdate(int i, String fieldName, String fieldValue) {
        Student student=studentDatabaseMap.get(i);
        student.setFirstName(fieldName);
        if (fieldName.toLowerCase().equals("firstname")) {
            student.setFirstName(fieldValue);
            System.out.println("student firstname is Updated");
        } else if (fieldName.toLowerCase().equals("lastname")) {
            student.setLastName(fieldValue);
            System.out.println("student lastname is Updated");
        } else if (fieldName.toLowerCase().equals("contactno")) {
            student.setContactNo(fieldValue);
            System.out.println("student contactno is Updated");
        }
       /* for (Student s : studentDatabase) {
            if (s.getRollNo() == i) {
                if (fieldName.toLowerCase().equals("firstname")) {
                    s.setFirstName(fieldValue);
                    break;
                } else if (fieldName.equals("lastname")) {
                    s.setLastName(fieldValue);
                    break;
                } else if (fieldName.equals("contactno")) {
                    s.setContactNo(fieldValue);
                    break;
                }
            }
        }*/
        return true;
    }

    @Override
    public boolean delete(int index) {
        studentDatabaseMap.remove(index);
        System.out.println("Student record deleted");
        return true;
    }

    @Override
    public HashMap<Integer,Student> getAllStudent() {
        return studentDatabaseMap;
    }
}
