package com.brevitaz.training.collectionexercise.studentmanagement.model;

public class Student {
    private int rollNo;
    private String firstName;
    private String lastName;
    private String contactNo;
    public Student()
    {

    }
    public Student(int rollNo, String firstName, String lastName, String contactNo)
    {
        this.rollNo=rollNo;
        this.firstName=firstName;
        this.lastName=lastName;
        this.contactNo=contactNo;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
}
