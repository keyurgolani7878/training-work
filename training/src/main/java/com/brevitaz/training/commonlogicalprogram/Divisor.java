package com.brevitaz.training.commonlogicalprogram;

import java.util.Scanner;

/**
 * Write programm to find all divisors of given no.
 * eg 1. No : 50
 * expected output : 1,2,5,10,25
 * eg 2. No : 16
 * expected output : 1,2,4,8
 * @author Keyur Golani
 */


public class Divisor {
    public static void main(String[] args) {
        divisor();
    }

    public static void divisor() {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        for (int i = 1; i <= k / 2; i++) {
            if (k % i == 0) {
                System.out.print(" " + i);
            }
        }
    }
}