package com.brevitaz.training.commonlogicalprogram;


import java.util.Scanner;

/**
 * Write a method that returns 'Fizz' for multiples of three and 'Buzz’ for the multiples of five. For numbers
 * which are multiples of both three and five return 'FizzBuzz'.For numbers that are neither, return the input number.
 * @author Keyur Golani
 */
public class FizzBuzz {
    public static void main(String[] args) {
        System.out.println(fizzbuzz());
    }

    public static String fizzbuzz() {
        System.out.println("Enter the number");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        if (number % 3 == 0 && number % 5 == 0) {
            return "FizzBuzz";
        } else if (number % 3 == 0) {
            return "Fizz";
        } else if (number % 5 == 0) {
            return "Buzz";
        }
        return Integer.toString(number);
    }
}
