package com.brevitaz.training.commonlogicalprogram;

import java.util.Arrays;

/**
 * Write a program in Java to make such a pattern like a pyramid with a number that will repeat the number in the
 * <p>
 * same row. Go to the editor
 * 1
 * 2 2
 * 3 3 3
 * 4 4 4 4
 * @author Keyur Golani
 */

public class Pattern {
    public static void main(String[] args) {
        int i, j;
        for (i = 1; i <= 4; i++) {
            for (int k = 4 - i; k >= 1; k--) {
                System.out.print(" ");
            }
            for (j = 0; j < i; j++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
