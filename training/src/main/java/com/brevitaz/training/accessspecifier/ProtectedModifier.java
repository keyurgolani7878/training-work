package com.brevitaz.training.accessspecifier;

public class ProtectedModifier {
    protected void show()
    {
        System.out.println("This can be accessed in the subclass of different package");
    }
}
