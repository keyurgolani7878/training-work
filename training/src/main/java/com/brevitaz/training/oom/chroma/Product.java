package com.brevitaz.training.oom.chroma;

public class Product {
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    private String model;

    Product(String model) {
        this.model = model;
    }
}
