package com.brevitaz.training.oom.vacation;

public class Path {
    private String via;

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    Path(String via)
    {
        setVia(via);
    }
}
