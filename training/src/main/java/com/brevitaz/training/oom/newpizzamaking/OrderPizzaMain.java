package com.brevitaz.training.oom.newpizzamaking;

import java.util.ArrayList;
import java.util.List;

public class OrderPizzaMain  {
    public static void main(String[] args) {
        Chef chef=new Chef();
        List<Toppings> toppings=new ArrayList<Toppings>();
        toppings.add(new Capsicum());
        List<Sauce> sauces=new ArrayList<>();
        sauces.add(new Tomato());
        sauces.add(new Taco());
        List<Cheese> cheeses=new ArrayList<>();
        cheeses.add(new Mozzarella());
        cheeses.add(new Mozzarella());
        PizzaStyle pizzaStyle=new PizzaStyle(sauces,cheeses,toppings,"thin crust");
        chef.makePizza(pizzaStyle);
    }
}
