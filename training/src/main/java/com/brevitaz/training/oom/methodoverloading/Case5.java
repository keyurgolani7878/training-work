package com.brevitaz.training.oom.methodoverloading;

public class Case5 {
    public void m1(int i) {
        System.out.println("int args");
    }

    public void m1(int... i) {
        System.out.println("int varargs method");
    }

    public static void main(String[] args) {
       Case5 case5=new Case5();
       case5.m1();
    }
}
