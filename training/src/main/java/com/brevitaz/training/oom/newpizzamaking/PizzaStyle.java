package com.brevitaz.training.oom.newpizzamaking;

import java.util.List;

public class PizzaStyle {
    private String baseType;
    private List<Sauce> sauceName;
    private List<Cheese> cheeseType;
    private List<Toppings> toppings;

    public PizzaStyle(List<Sauce> sauces, List<Cheese> cheeses, List<Toppings> toppings,String baseType) {
        this.baseType=baseType;
        this.sauceName = sauces;
        this.cheeseType = cheeses;
        this.toppings = toppings;
    }

    public List<Sauce> getSauceName() {
        return sauceName;
    }

    public List<Cheese> getCheese() {
        return cheeseType;
    }

    public List<Toppings> getToppings() {
        return toppings;
    }

    public String getBaseType()
    {
        return baseType;
    }


}
