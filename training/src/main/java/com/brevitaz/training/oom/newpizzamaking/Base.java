package com.brevitaz.training.oom.newpizzamaking;

import java.util.ArrayList;
import java.util.List;

public class Base implements Bakeable {

    String baseType;
    Dough dough;
    List<Cheese> cheeses = new ArrayList<>();
    List<Toppings> toppings = new ArrayList<>();
    List<Sauce> sauces = new ArrayList<>();

    public String getBaseType() {
        return baseType;
    }

    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    public List<Toppings> getToppings() {
        return toppings;
    }

    public void setToppings(List<Toppings> toppings) {
        this.toppings = toppings;
    }

    public List<Sauce> getSauces() {
        return sauces;
    }

    public void setSauces(List<Sauce> sauces) {
        this.sauces = sauces;
    }

    public List<Cheese> getCheeses() {
        return cheeses;
    }

    public void setCheeses(List<Cheese> cheeses) {
        this.cheeses = cheeses;
    }

    public Base(Dough dough) {
        this.dough = dough;
    }
    @Override
    public String toString() {
        return "Base{" +
                "baseType='" + baseType + '\'' +
                ", dough=" + dough +
                ", cheeses=" + cheeses +
                ", toppings=" + toppings +
                ", sauces=" + sauces +
                '}';
    }

    @Override
    public BakedFood bake() {
        System.out.println(" Pizza is being baked");
        System.out.println(" Pizza is ready");
        System.out.println(new Pizza(this));
        return new Pizza(this);
    }
}
