package com.brevitaz.training.oom.newpizzamaking;

public interface Bakeable {
   public BakedFood bake();
}
