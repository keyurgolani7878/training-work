package com.brevitaz.training.oom.pizzamaking;

import java.util.Scanner;

public class OrderPizza {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the type of pizza you want to have");
        System.out.println("1 for Margherita");
        System.out.println("2 for Veggie");
        int choice=scanner.nextInt();
        Chef chef=new Chef("Kunal");
        Pizza pizza;
        if (choice==1)
        {
            pizza=new Margherita();
        }
        else if (choice==2)
        {
            pizza=new Veggie();
        }
        else
        {
            pizza=new Pizza();
        }
        pizza.setBaseType("thincrust");
        pizza.setCheeseType("Mozzerella");
        pizza.setNumberOfToppings(4);
        chef.prepare(pizza);
        chef.bake();
        chef.cut(4);
        Waiter waiter=new Waiter("Sanjeev");
        waiter.serve();


    }
}
