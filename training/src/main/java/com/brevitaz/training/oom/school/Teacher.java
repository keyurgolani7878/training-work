package com.brevitaz.training.oom.school;

public class Teacher extends Person {
    private String subject = "java";

    public void teach(Student s) {
        System.out.println(getName() + " is teaching " + subject + " to " + s.getName());
    }
}
