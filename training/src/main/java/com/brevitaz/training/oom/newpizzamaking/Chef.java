package com.brevitaz.training.oom.newpizzamaking;

import java.util.ArrayList;
import java.util.List;

public class Chef {
    List<DoughIngredients> doughIngredients = new ArrayList<>();

    {
        doughIngredients.add(new WhiteFlore());
        doughIngredients.add(new Yeast());
        doughIngredients.add(new Salt());
        doughIngredients.add(new Water());
        doughIngredients.add(new Oil());
    }

    List<Toppings> toppings = new ArrayList<>();

    {
        toppings.add(new Capsicum());
        toppings.add(new Corn());
        toppings.add(new Paneer());
    }

    List<Sauce> sauces = new ArrayList<>();

    {
        sauces.add(new Pesto());
        sauces.add(new Taco());
        sauces.add(new Tomato());
    }
    List<Cheese> cheeses=new ArrayList<>();
    {
        cheeses.add(new Mozzarella());
        cheeses.add(new Cheddar());
    }

    public Dough makeDough(List<DoughIngredients> doughIngredients) {
        System.out.print(" 1.Mixing ");
        for (DoughIngredients doughIngredients1 : doughIngredients) {
            System.out.print(doughIngredients1 + " ");
        }
        Dough dough = new Dough(doughIngredients);
        System.out.println("");
        System.out.println(" 2.Dough is ready");
        return dough;

    }

    public Base rollDough(Dough dough) {
        System.out.println(" 3.Rolling dough");
        System.out.println(" 4.Base is ready ");
        return new Base(dough);
    }

    public Pizza makePizza(PizzaStyle pizzaStyle) {
        Dough dough = this.makeDough(doughIngredients);
        Base base = this.rollDough(dough);
        base.setBaseType(pizzaStyle.getBaseType());
        spreadSauces(base, pizzaStyle.getSauceName());

        System.out.print(" 5.Spreading ");
        for (Sauce sauce : pizzaStyle.getSauceName()) {
            System.out.print(sauce + " ");
        }
        addToppings(base, pizzaStyle.getToppings());
        System.out.println("");
        System.out.print(" 6.Adding ");
        for (Toppings toppings : pizzaStyle.getToppings()) {
            System.out.print(toppings);
        }
        spreadCheese(base,pizzaStyle.getCheese());
       // Cheese cheese = new Cheese();
       // System.out.println("With " + pizzaStyle.getCheeseQuantity() + cheese.toString());
        Pizza pizza = bakeFood(base);
        return pizza;
    }

    public void spreadSauces(Base base, List<Sauce> sauces) {
        base.setSauces(sauces);
    }

    public void addToppings(Base base, List<Toppings> toppings) {
        base.setToppings(toppings);
    }
    public void spreadCheese(Base base,List<Cheese> cheese)
    {
        base.setCheeses(cheese);
    }

    public Pizza bakeFood(Base base) {
        Oven oven = new Oven();
        Pizza pizza = (Pizza) oven.bake(base);
        return pizza;
    }

}
