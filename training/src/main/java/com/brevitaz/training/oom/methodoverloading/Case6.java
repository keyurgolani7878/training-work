package com.brevitaz.training.oom.methodoverloading;

public class Case6 {
    public void m1(Animal animal)
    {
        System.out.println("Animal version");
    }
    public void m1(Monkey monkey)
    {
        System.out.println("Monkey version");
    }

    public static void main(String[] args) {
        Animal a=new Animal();
        Monkey m=new Monkey();
        Animal am=new Monkey();
        Case6 case6=new Case6();
        case6.m1(a);
        case6.m1(m);
        case6.m1(am);
    }
}
class Monkey extends Animal{

}
class Animal{

}
