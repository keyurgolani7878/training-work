package com.brevitaz.training.oom.cafe;

public class CafeMain {
    public static void main(String[] args) {
        Servant servant = new Servant();
        servant.setName("Prakash");
        Beverages beverages = new Beverages();
        beverages.setName("Tea");
        servant.Serve(beverages);

    }
}
