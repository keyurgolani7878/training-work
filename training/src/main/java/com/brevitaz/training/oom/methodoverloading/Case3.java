package com.brevitaz.training.oom.methodoverloading;

public class Case3 {
    public void m1(String s)
    {
        System.out.println("String version");
    }
    public void m1(StringBuffer b)
    {
        System.out.println("String buffer version");
    }

    public static void main(String[] args) {
      Case3 case3=new Case3();
        case3.m1("keyur");
       // this will give error  case3.m1(null);
    }
}
