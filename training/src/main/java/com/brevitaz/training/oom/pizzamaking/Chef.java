package com.brevitaz.training.oom.pizzamaking;

public class Chef extends Person {
    public Chef(String name)
    {
        this.name=name;
        System.out.println(name+" has already prepared a dough using ingredients");
    }

    public void prepare(Pizza pizza)
    {
        System.out.println(name+" is preparing" +pizza.toString()+" pizza with "+pizza.getBaseType()+" and using "+pizza.getCheeseType()+" and also adds "+pizza.getNumberOfToppings()+" toppings on it ");
    }
    public void bake()
    {
        System.out.println("Pizza is being baked");
    }
    public void cut(int pieces)
    {
        System.out.println("Pizza cutted into "+pieces);
    }

}
