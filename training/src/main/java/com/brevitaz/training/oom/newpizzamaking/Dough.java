package com.brevitaz.training.oom.newpizzamaking;

import java.util.List;

public class Dough {
    public List<DoughIngredients> doughIngredients;

    Dough(List<DoughIngredients> doughIngredients) {
        this.doughIngredients = doughIngredients;
    }

    public List<DoughIngredients> getDoughIngredients() {
        return doughIngredients;
    }

    public void setDoughIngredients(List<DoughIngredients> doughIngredients) {
        this.doughIngredients = doughIngredients;
    }

    @Override
    public String toString() {
        return "Dough{" +
                "doughIngredients=" + doughIngredients +
                '}';
    }
}
