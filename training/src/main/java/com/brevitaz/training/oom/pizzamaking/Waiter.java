package com.brevitaz.training.oom.pizzamaking;

public class Waiter extends Person {
    Waiter(String name)
    {
        this.name=name;
    }
    public void serve()
    {
        System.out.println("Pizza is being served by "+name);
    }
}
