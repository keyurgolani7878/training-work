package com.brevitaz.training.oom.school;

public class SchoolMain {
    public static void main(String[] args) {
        Teacher t = new Teacher();
        Student s = new Student();
        t.setName("Kapil");
        s.setName("Sunil");
        t.teach(s);
        // System.out.println(t.name+" is teachning "+"Subject to "+s.name);
    }
}
