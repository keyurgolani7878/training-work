package com.brevitaz.training.oom.methodoverloading;

public class Case1 {
    public void m1(int i)
    {
        System.out.println("int args");
    }
    public void m1(float f)
    {
        System.out.println("float args");
    }

    public static void main(String[] args) {
        Case1 case1=new Case1();
        case1.m1(4);
        case1.m1('a');
        //this will give compile time error case1.m1(4.5);
    }


}
