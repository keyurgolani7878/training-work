package com.brevitaz.training.oom.vacation;

public class Car extends Vehicle {
    Car(String name, String registrationNumber)
    {
        setName(name);
        setRegistrationNumber(registrationNumber);
    }
}
