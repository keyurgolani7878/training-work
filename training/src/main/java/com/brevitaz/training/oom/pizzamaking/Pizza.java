package com.brevitaz.training.oom.pizzamaking;

public class Pizza {
    private String baseType;
    private String cheeseType;
    private int numberOfToppings;

    public String getBaseType() {
        return baseType;
    }

    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    public String getCheeseType() {
        return cheeseType;
    }

    public void setCheeseType(String cheeseType) {
        this.cheeseType = cheeseType;
    }

    public int getNumberOfToppings() {
        return numberOfToppings;
    }

    public void setNumberOfToppings(int numberOfToppings) {
        this.numberOfToppings = numberOfToppings;
    }

    @Override
    public String toString() {
        return "Pizza{}";
    }


}
