package com.brevitaz.training.oom.cafe;

public class Servant extends Person {


    public void Serve(Beverages beverages) {
        System.out.println(getName() + " is serving " + beverages.getName());
    }
}
