package com.brevitaz.training.oom.chroma;

public class ChromaMain {
    public static void main(String[] args) {
        Customer customer = new Customer("Student");
        Salesman salesman = new Salesman("Bill");
        Product product = new Product("Laptop");
        salesman.sale(product, customer);
    }
}
