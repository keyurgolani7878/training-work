package com.brevitaz.training.oom.chroma;

public class Salesman extends Person {
    Salesman(String name) {
        //this.name = name;
        setName(name);
    }

    public void sale(Product product, Customer customer) {
        System.out.println(getName() + " sells " + product.getModel() + " to " + customer.getName() + " at low price");
    }
}
