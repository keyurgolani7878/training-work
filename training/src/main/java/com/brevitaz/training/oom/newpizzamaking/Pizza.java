package com.brevitaz.training.oom.newpizzamaking;

public class Pizza implements BakedFood {
    Base base;

    public Pizza(Base base) {
        this.base=base;
    }

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "base=" + base +
                '}';
    }
}
