package com.brevitaz.training.oom.newpizzamaking;

public class Mozzarella extends Cheese {
    @Override
    public String toString() {
        return "Mozzarella ";
    }
}
