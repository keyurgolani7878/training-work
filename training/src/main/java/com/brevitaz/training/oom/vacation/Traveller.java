package com.brevitaz.training.oom.vacation;

public class Traveller extends Person {
    Traveller(String name) {
       setName(name);
    }

    public void Travels(Location location,Car car,Path path) {
        System.out.println(getName()+" is travelling to "+location.getDestination()+" in "+car.getName()+" with number "+car.getRegistrationNumber()+" via "+path.getVia());
    }
}
