package com.brevitaz.training.oom.methodoverloading;

public class Case2 {
    public void m1(String s)
    {
        System.out.println("String version");
    }
    public void m1(Object o)
    {
        System.out.println("object version");
    }

    public static void main(String[] args) {
        Case2 case2=new Case2();
        case2.m1(new Object());
        case2.m1("keyur");
        case2.m1(null);
    }
}
