package com.brevitaz.training.oom.methodoverloading;

public class Case4 {
    public void m1(int i,float f)
    {
        System.out.println("int-float version");
    }
    public void m1(float f,int i)
    {
        System.out.println("float-int version");
    }

    public static void main(String[] args) {
        Case4 case4=new Case4();
        case4.m1(1,10.5f);
        case4.m1(10.5f,1);
        // this will give error case4.m1(1,1);
    }
}
